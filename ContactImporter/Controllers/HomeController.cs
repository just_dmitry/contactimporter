﻿using ContactImporter.Controllers.Params;
using ContactImporter.Data;
using ContactImporter.Data.Entities;
using ContactImporter.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.Linq;

namespace ContactImporter.Controllers
{
    public class HomeController : Controller
    {
        private DaoFactory Dao { get; set; }
        private ImportHelper ImportHelper { get; set; }
        private IStringLocalizer<HomeController> Localizer;
        public HomeController(IStringLocalizer<HomeController> localizer, DaoFactory dao, ImportHelper importHelper)
        {
            Dao = dao;
            Localizer = localizer;
            ImportHelper = importHelper;
        }
        public IActionResult Index()
        {
            ViewData["IsMobile"] = Utils.BrowserIsMobile(HttpContext);//не используется, но может когда-то пригодиться
            //var viewModel = Dao.Contact.GetAll().Include(c => c.Address).OrderByDescending(c => c.ID).Take(100).ToList();
            return View();
        }
        public IActionResult Import()
        {
            ViewData["IsMobile"] = Utils.BrowserIsMobile(HttpContext);//не используется, но может когда-то пригодиться
            FileProcessStats fileProcessStats = null;
            //Проверяем есть ли в сессии файл, который сейчас обрабатывается
            //если есть, то передать во View статус обработки в виде экземпляра класса FileProcessStats
            var currentFileProcessStatsJson = GetCurrentFileProcessStats();
            if (!string.IsNullOrEmpty(currentFileProcessStatsJson))
            {
                fileProcessStats = JsonConvert.DeserializeObject<FileProcessStats>(currentFileProcessStatsJson);
            }
            return View(fileProcessStats);
        }
        [HttpPost]
        public string GetProcessStats()
        {
            var result = GetCurrentFileProcessStats();
            return result;
        }
        [HttpPost]
        public void Upload()
        {
            var fileForm = Request.Form.Files[0];
            using (var stream = fileForm.OpenReadStream())
            {
                ImportHelper.ExcelPackage = new ExcelPackage(stream);
                ImportHelper.StartImport(HttpContext, fileForm.FileName);
            }
        }
        [HttpPost]
        public string ProcessTable(JQDTParams filter)
        {
            var recordsTotal = Dao.Contact.GetAll().Count();
            IQueryable<Contact> data = Dao.Contact.GetAll().Include(c => c.Address);
            if (filter.order[0].dir.ToString().Equals(JQDTColumnOrderDirection.asc.ToString()))
            {
                if (filter.order[0].column == 0) data = data.OrderBy(c => c.ID);
                 else data = data.OrderBy(c => c.FullName);
            }
            else
            {
                if (filter.order[0].column == 0) data = data.OrderByDescending(c => c.ID);
                else data = data.OrderByDescending(c => c.FullName);
            };
            var result = data.Skip(filter.start).Take(filter.length).ToList();
            var resultArray = result.Select(
                c => new [] {
                    c.ID.ToString(),
                    c.FullName,
                    c.PhoneNumber,
                    c.Email,
                    c.Address != null ? c.Address.PostIndex : "",
                    c.Address != null ? c.Address.Region : "",
                    c.Address != null ? c.Address.City : "",
                    c.Address != null ? c.Address.CityAddress : ""
                }).ToList();

            var jqdtObject = new
            {
                draw = filter.draw,
                recordsTotal = recordsTotal,
                recordsFiltered = recordsTotal,
                data = resultArray
            };
            return JsonConvert.SerializeObject(jqdtObject);
        }
        /// <summary>
        /// Берет из коллекции статуса обрабатываемых файлов статус обработки файла текущей сессии
        /// </summary>
        /// <returns>Возвращает текущий статус в виде json</returns>
        private string GetCurrentFileProcessStats()
        {
            var currentFileGuid = SessionHelper.GetCurrentFileGuid(HttpContext);
            if (!string.IsNullOrEmpty(currentFileGuid))
            {
                var currentFileStats = Dao.FileProcessStats.GetByGuid(currentFileGuid);
                return JsonConvert.SerializeObject(currentFileStats);
            }
            return null;
        }
    }
}