﻿using System.Collections.Generic;

namespace ContactImporter.Controllers.Params
{
    /// <summary>
    /// Модель для фильтра JQuery DataTable plugin
    /// </summary>
    public class JQDTParams
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public List<JQDTColumnOrder> order { get; set; }
    }
    public enum JQDTColumnOrderDirection
    {
        asc, desc
    }
    public class JQDTColumnOrder
    {
        public int column { get; set; }
        public JQDTColumnOrderDirection dir { get; set; }
    }
}