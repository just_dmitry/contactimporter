﻿$(document).ready(function () {
    fileWorker.init();
});
var fileWorker = {
    file: null,
    statsInterval: null,
    init: function () {
        fileWorker.cacheElements();
        fileWorker.bindEvents();
        if (fileIsProcess == 1) fileWorker.startProcessObservation();
    },
    cacheElements: function () {
        fileWorker.uploadFileElements = '#upload-file-elements';
        fileWorker.fileElem = '#file-input';
        fileWorker.loadProgress = '#upload-progress';
        fileWorker.startUploadBtn = '#start-upload-btn';

        fileWorker.processLoadingIcon = '#process-loading-icon';
        fileWorker.fileProcessFinishedTitle = '#file-process-finished-title';
        fileWorker.fileIsProcessingTitle = '#file-is-processing-title';
        fileWorker.uploadNewFileBtn = '#upload-new-file';
        fileWorker.processObserverZone = '#process-observer';

        fileWorker.rowCount = '#row-count span';
        fileWorker.processedRowCount = '#processed-row-count span';
        fileWorker.successProcessedRowCount = '#success-processed-row-count span';
        fileWorker.uncorrectProcessedRowCount = '#uncorrect-processed-row-count span';
        fileWorker.extraColumns = '#extra-columns span';
        fileWorker.missingColumns = '#missing-columns span';
        fileWorker.missingRequiredColumns = '#missing-required-columns span';
    },
    bindEvents: function () {

    },
    startUpload: function () {
        var files = $(fileWorker.fileElem)[0].files;
        if (files.length <= 0) alert('Please, select an excel file.');
        else {
            for (var i = 0; i < files.length; i++) {
                fileWorker.file = files[i];
            }
            fileWorker.uploadTheImage();
        }
    },
    uploadTheImage: function () {
        $(fileWorker.fileElem).hide();
        $(fileWorker.loadProgress).show();
        var formData = new FormData();
        formData.append('file', fileWorker.file);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'Upload');
        xhr.upload.onprogress = function (event) {
            if (event.lengthComputable) {
                var complete = (event.loaded / event.total * 100 | 0);
                $(fileWorker.loadProgress).html(complete + '%');
            }
        };
        xhr.onload = function () {
            $(fileWorker.loadProgress).html('uploading finished');
            fileWorker.startProcessObservation();
        };
        xhr.send(formData);
    },
    startProcessObservation: function () {
        $(fileWorker.fileElem).hide();
        $(fileWorker.loadProgress).hide();
        $(fileWorker.startUploadBtn).hide();
        $(fileWorker.processObserverZone).show();
        $(fileWorker.processLoadingIcon).show();
        $(fileWorker.fileProcessFinishedTitle).hide();
        $(fileWorker.fileIsProcessingTitle).show();
        fileWorker.statsInterval = setInterval(function () {
            fileWorker.loadProcessStats();
        }, 3000);
    },
    loadProcessStats: function() {
        $.ajax({
            url: 'GetProcessStats',
            type: "POST",
            dataType: 'json',
            success: function (data) {
                fileWorker.updateProcessStats(data);
            }
        });
    },
    updateProcessStats: function (data) {
        $(fileWorker.rowCount).html(data.RowCount);
        $(fileWorker.processedRowCount).html(data.ProcessedRowCount);
        $(fileWorker.successProcessedRowCount).html(data.SuccessProcessedRowCount);
        $(fileWorker.uncorrectProcessedRowCount).html(data.UncorrectProcessedRowCount);
        $(fileWorker.extraColumns).html(data.ExtraColumns);
        $(fileWorker.missingColumns).html(data.MissingColumns);
        $(fileWorker.missingRequiredColumns).html(data.MissingRequiredColumns);
        if (data.IsProcessFinished == 1) {
            $(fileWorker.processLoadingIcon).hide();
            $(fileWorker.fileProcessFinishedTitle).show();
            $(fileWorker.fileIsProcessingTitle).hide();
            $(fileWorker.uploadNewFileBtn).show();
            clearInterval(fileWorker.statsInterval);
        }
    },
    clearProcessStats: function () {
        $(fileWorker.rowCount).html('--');
        $(fileWorker.processedRowCount).html('--');
        $(fileWorker.successProcessedRowCount).html('--');
        $(fileWorker.uncorrectProcessedRowCount).html('--');
        $(fileWorker.extraColumns).html('--');
        $(fileWorker.missingColumns).html('--');
        $(fileWorker.missingRequiredColumns).html('--');
    },
    uploadNewFile: function () {
        //hide process elements
        $(fileWorker.processObserverZone).hide();
        $(fileWorker.processLoadingIcon).show();
        $(fileWorker.fileProcessFinishedTitle).hide();
        $(fileWorker.fileIsProcessingTitle).show();
        $(fileWorker.uploadNewFileBtn).hide();

        //show upload elements
        $(fileWorker.fileElem).show();
        $(fileWorker.loadProgress).hide();
        $(fileWorker.startUploadBtn).show();
        $(fileWorker.uploadFileElements).show();

        fileWorker.clearProcessStats();
        clearInterval(fileWorker.statsInterval);
    }
}