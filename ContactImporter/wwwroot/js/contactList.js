﻿$(document).ready(function () {
    $('#contact-list').DataTable({
        serverSide: true,
        ajax: {
            url: 'Home/ProcessTable',
            type: 'POST'
        },
        "columnDefs": [
            { "orderable": false, "targets": [2,3,4,5,6,7] }
        ],
        'info': false,
        'searching': false
    });
});