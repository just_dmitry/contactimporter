﻿using ContactImporter.Data;
using ContactImporter.Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ContactImporter.Helper
{
    public class ImportHelper
    {
        private readonly IOptions<ImportFileConfig> Config;
        private readonly IOptions<DBConfig> DBConfig;
        public ExcelPackage ExcelPackage { get; set; }
        /// <summary>
        /// Содержит названия всех нужных колонок и номер этой колонки в импортируемом файле
        /// </summary>
        private IDictionary<string, int> ColumnNameNumbers { get; set; }
        private int RowCount { get; set; }
        private int ColumnCount { get; set; }
        private FileProcessStats FileProcessStats { get; set; }
        private DaoFactory Dao { get; set; }
        public ImportHelper(DaoFactory dao, IOptions<ImportFileConfig> config, IOptions<DBConfig> dbConfig)
        {
            Dao = dao;
            //Устанавливаем сначала все номера колонок в 0
            ColumnNameNumbers = new Dictionary<string, int> {
                { "FullName", 0 },
                { "PhoneNumber", 0 },
                { "Email", 0 },
                { "PostIndex", 0 },
                { "Region", 0 },
                { "City", 0 },
                { "CityAddress", 0 }
            };
            FileProcessStats = new FileProcessStats();
            Config = config;
            DBConfig = dbConfig;
        }
        public void StartImport(HttpContext httpContext, string fileName)
        {
            ExcelWorksheet workSheet = ExcelPackage.Workbook.Worksheets[1];
            RowCount = workSheet.Dimension.End.Row;
            ColumnCount = workSheet.Dimension.End.Column;
            FileProcessStats.RowCount = RowCount - 1;
            FileProcessStats.FileName = fileName;
            Dao.FileProcessStats.Insert(FileProcessStats);
            SetColumnIndexes(workSheet);
            if (FileProcessStats.MissingRequiredColumns.Count() > 0)
            {
                //Если не хватает обязательной колонки в импортируемом файле, то установить флаг - обработка завершена.
                FileProcessStats.IsProcessFinished = 1;
                Dao.FileProcessStats.Update(FileProcessStats);
                SessionHelper.SetCurrentFileGuid(httpContext, FileProcessStats.Guid.ToString());
                return;
            }
            var thread = new Thread(() => ProcessFile(FileProcessStats.Guid.ToString(), workSheet, 2, RowCount))
            {
                Name = "FileProcessThread"
            };
            thread.Start();
            Dao.FileProcessStats.Update(FileProcessStats);
            SessionHelper.SetCurrentFileGuid(httpContext, FileProcessStats.Guid.ToString());
        }
        private void ProcessFile(string fileGuid, ExcelWorksheet workSheet, int startRow, int endRow)
        {
            //Ввиду того, что DbContext-ы из основного потока диспозятся пока выполняется этот поток
            //приходится тут вручную создавать экземпляр DBCobtext-а
            var optionsBuilder = new DbContextOptionsBuilder<ContactImporterDBContext>();
            optionsBuilder.UseSqlServer(DBConfig.Value.ContactImporterDBConnectionString);
            var dbContext = new ContactImporterDBContext(optionsBuilder.Options);
            using (var dao = new DaoFactory(new Data.Dao.ContactDao(dbContext), new Data.Dao.AddressDao(dbContext), new Data.Dao.FileProcessStatsDao(dbContext)))
            {
                for (int i = startRow; i <= endRow; i++)
                {
                    var contact = new Contact();
                    var address = new Address();
                    for (int j = 1; j <= workSheet.Dimension.End.Column; j++)
                    {
                        var columnName = ColumnNameNumbers.FirstOrDefault(c => c.Value == j).Key;
                        switch (columnName)
                        {
                            case "FullName":
                                {
                                    var value = (string)workSheet.Cells[i, j].Value;
                                    contact.FullName = ContactValidationHelper.ValidateFullName(value);
                                    break;
                                }
                            case "PhoneNumber":
                                {
                                    var value = (string)workSheet.Cells[i, j].Value;
                                    contact.PhoneNumber = ContactValidationHelper.ValidatePhoneNumber(value);
                                    break;
                                }
                            case "Email":
                                {
                                    var value = (string)workSheet.Cells[i, j].Value;
                                    contact.Email = ContactValidationHelper.ValidateEmail(value);
                                    break;
                                }
                            case "PostIndex":
                                {
                                    address.PostIndex = (string)workSheet.Cells[i, j].Value;
                                    break;
                                }
                            case "Region":
                                {
                                    address.Region = (string)workSheet.Cells[i, j].Value;
                                    break;
                                }
                            case "City":
                                {
                                    address.City = (string)workSheet.Cells[i, j].Value;
                                    break;
                                }
                            case "CityAddress":
                                {
                                    address.CityAddress = (string)workSheet.Cells[i, j].Value;
                                    break;
                                }
                            default:
                                {
                                    continue;
                                }
                        }
                    }
                    if (ContactValidationHelper.IsValidContact(contact))
                    {
                        if (ContactValidationHelper.IsValidAddress(address))
                        {
                            contact.Address = address;
                        }
                        //Если такой номер телефона уже есть в базе
                        var newContact = dao.Contact.GetContactByPhoneNumber(contact.PhoneNumber);
                        if (newContact != null)
                        {
                            //обновить в загруженном объекте данные на новые
                            newContact.GetCopyFrom(contact);
                        }
                        else
                        {
                            newContact = contact;
                        }
                        if (newContact.Address != null)
                        {
                            if (newContact.Address.ID != 0)
                            {
                                //Если адрес был, то его обновляем
                                dao.Address.Update(newContact.Address);
                            }
                            else
                            {
                                //Если не было, то добавляем
                                dao.Address.Insert(newContact.Address);
                            }
                        }
                        var isSuccess = false;
                        if (newContact.ID != 0)
                        {
                            isSuccess = dao.Contact.Update(newContact);
                        }
                        else
                        {
                            isSuccess = dao.Contact.Insert(newContact);
                        }

                        if (isSuccess)
                        {
                            FileProcessStats.SuccessProcessedRowCount++;
                        }
                        else
                        {
                            FileProcessStats.UncorrectProcessedRowCount++;
                        }
                    }
                    else
                    {
                        //Какой-то из обязательных параметров не прошел валидацию
                        FileProcessStats.UncorrectProcessedRowCount++;
                    }
                    FileProcessStats.ProcessedRowCount++;
                    dao.FileProcessStats.Update(FileProcessStats);
                }
                FileProcessStats.IsProcessFinished = 1;
                dao.FileProcessStats.Update(FileProcessStats);
            }
        }
        /// <summary>
        /// Устанавливает каким номером идет та или иная колонка в импортируемом файле.
        /// Выявляет каких колонок не хватает и какие из них обязательны.
        /// </summary>
        private void SetColumnIndexes(ExcelWorksheet workSheet)
        {
            var extraColumns = new List<string>();
            for (var i = 1; i <= ColumnCount; i++)
            {
                var columnName = workSheet.Cells[1, i].Value;
                if (columnName == null) continue;
                var columnNameStr = columnName.ToString();
                if (ColumnNameNumbers.ContainsKey(columnNameStr))
                {
                    ColumnNameNumbers[columnNameStr] = i;
                } else
                {
                    //Если колонки нет в списке, значит это лишняя колонка
                    extraColumns.Add(columnNameStr);
                }
            }
            FileProcessStats.ExtraColumns = string.Join(",", extraColumns);
            //Если индекс колонки остался 0, значит такой колонки нет в импортируемом файле
            var missingColumns = ColumnNameNumbers.Where(c => c.Value == 0).Select(c => c.Key);
            if (missingColumns != null && missingColumns.Count() != 0)
            {
                FileProcessStats.MissingColumns = string.Join(",", missingColumns);
                var missionRequiredColumns = missingColumns.Where(c => Config.Value.RequiredColumns.Contains(c));
                if (missionRequiredColumns != null && missionRequiredColumns.Count() != 0)
                {
                    FileProcessStats.MissingRequiredColumns = string.Join(",", missionRequiredColumns);
                }
            }
        }
    }
}
