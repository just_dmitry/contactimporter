﻿using ContactImporter.Data.Entities;
using System.Text.RegularExpressions;

namespace ContactImporter.Helper
{
    public class ContactValidationHelper
    {
        public static string ValidateFullName(string fullname)
        {
            var result = fullname;
            if (string.IsNullOrEmpty(fullname)) result = null;
            else
            {
                Regex regex = new Regex("[ ]{2,}");
                result = regex.Replace(result, " ");
            }
            return result;
        }
        public static string ValidatePhoneNumber(string phoneNumber)
        {
            string result = null;
            if (string.IsNullOrEmpty(phoneNumber)) result = null;
            else
            {
                string pattern = @"((\+{1}7{1})|8) ?\(?( ?9[0-9]{2} ?)\)? ?-? ?([0-9]{3}) ?-? ?([0-9]{2}) ?-? ?([0-9]{2})";
                MatchCollection matches = Regex.Matches(phoneNumber, pattern);
                foreach (Match match in matches)
                {
                    result = match.Groups[3].Value.Trim() + match.Groups[4].Value.Trim() + match.Groups[5].Value.Trim() + match.Groups[6].Value.Trim();
                }
            }
            return result;
        }
        public static string ValidateEmail(string email)
        {
            string result = null;
            if (string.IsNullOrEmpty(email)) result = null;
            else
            {
                string pattern = @"^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$";
                var rgx = new Regex(pattern);
                if (rgx.Match(email.ToLower()).Success)
                {
                    result = email;
                }
            }
            return result;
        }
        public static bool IsValidContact(Contact contact)
        {
            return contact.FullName != null && contact.PhoneNumber != null && contact.Email != null;
        }
        public static bool IsValidAddress(Address address)
        {
            return address.PostIndex != null && address.Region != null && address.City != null && address.CityAddress != null;
        }
    }
}