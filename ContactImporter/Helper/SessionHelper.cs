﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ContactImporter.Helper
{
    public class SessionHelper
    {
        public static void SetCurrentFileGuid(HttpContext context, string fileGuid)
        {
            context.Session.SetString("CurrentFileGuid", fileGuid);
        }
        public static string GetCurrentFileGuid(HttpContext context)
        {
            return context.Session.GetString("CurrentFileGuid");
        }
        public static void RemoveCurrentFileGuid(HttpContext context)
        {
            context.Session.Remove("CurrentFileGuid");
        }
    }
}
