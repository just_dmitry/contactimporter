﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ContactImporter.Data.Entities
{
    public class Contact: Entity
    {
        [Required]
        public string FullName { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Email { get; set; }
        public long? AddressId { get; set; }
        public Address Address { get; set; }

        public void GetCopyFrom(Contact contact)
        {
            FullName = contact.FullName;
            PhoneNumber = contact.PhoneNumber;
            Email = contact.Email;
            if (contact.Address == null)
            {
                Address = null;
                AddressId = null;
            }
            else if (Address == null)
            {
                Address = contact.Address;
            }
            else
            {
                Address.PostIndex = contact.Address.PostIndex;
                Address.Region = contact.Address.Region;
                Address.City = contact.Address.City;
                Address.CityAddress = contact.Address.CityAddress;
            }
        }
    }
}