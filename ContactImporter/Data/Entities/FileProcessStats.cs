﻿using System;
using System.Collections.Generic;

namespace ContactImporter.Data.Entities
{
    /// <summary>
    /// Класс для хранения текущего состояния процесса импорта.
    /// Это состояние обновляется по мере необходимости и постоянно запрашивается клиентом.
    /// </summary>
    public class FileProcessStats: Entity
    {
        public Guid Guid { get; set; }
        public string FileName { get; set; }
        public int RowCount { get; set; }
        public int ProcessedRowCount { get; set; }
        public int SuccessProcessedRowCount { get; set; }
        public int UncorrectProcessedRowCount { get; set; }
        public int IsProcessFinished { get; set; }
        public string ExtraColumns { get; set; }
        public string MissingColumns { get; set; }
        public string MissingRequiredColumns { get; set; }
        public FileProcessStats()
        {
            Guid = Guid.NewGuid();
            RowCount = 0;
            ProcessedRowCount = 0;
            SuccessProcessedRowCount = 0;
            UncorrectProcessedRowCount = 0;
            IsProcessFinished = 0;
            ExtraColumns = "";
            MissingColumns = "";
            MissingRequiredColumns = "";
        }
    }
}