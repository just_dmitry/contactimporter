﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ContactImporter.Data.Entities
{
    public class Address: Entity
    {
        public string PostIndex { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string CityAddress { get; set; }
        [InverseProperty("Address")]
        public IList<Contact> Contacts { get; set; }
    }
}
