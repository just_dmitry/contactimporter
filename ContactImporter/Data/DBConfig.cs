﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactImporter.Data
{
    /// <summary>
    /// Класс для Inject-а ConnectionString из appsettings.json
    /// Используется для инициализации объектов DBContext-а в неосновных потоках.
    /// </summary>
    public class DBConfig
    {
        public string ContactImporterDBConnectionString { get; set; }
    }
}
