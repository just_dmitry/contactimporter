﻿
using ContactImporter.Data.Dao;
using System;

namespace ContactImporter.Data
{
    public class DaoFactory: IDisposable
    {
        public ContactDao Contact { get; set; }
        public AddressDao Address { get; set; }
        public FileProcessStatsDao FileProcessStats { get; set; }

        public DaoFactory(ContactDao contact, AddressDao address, FileProcessStatsDao fileProcessStats)
        {
            Contact = contact;
            Address = address;
            FileProcessStats = fileProcessStats;
        }

        public void Dispose()
        {
            Contact.Dispose();
            Address.Dispose();
            FileProcessStats.Dispose();
        }
    }
}