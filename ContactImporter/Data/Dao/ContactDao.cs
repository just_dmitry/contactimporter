﻿using ContactImporter.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ContactImporter.Data.Dao
{
    public class ContactDao : BaseDao<Contact>, IDisposable
    {
        public ContactDao(DbContext _context) : base(_context)
        {
        }
        public Contact GetContactByPhoneNumber(string phoneNumber)
        {
            var result = Table.Include(c => c.Address).FirstOrDefault(p => phoneNumber.Equals(p.PhoneNumber));
            return result;
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}