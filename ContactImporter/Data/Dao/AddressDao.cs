﻿using ContactImporter.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace ContactImporter.Data.Dao
{
    public class AddressDao : BaseDao<Address>, IDisposable
    {
        public AddressDao(DbContext _context) : base(_context)
        {
        }
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}