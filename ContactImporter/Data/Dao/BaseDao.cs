﻿using ContactImporter.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactImporter.Data.Dao
{
    public abstract class BaseDao<T> where T: Entity
    {
        protected DbSet<T> Table { get; set; }
        protected DbContext Context { get; set; }
        private int SaveChanges()
        {
            return Context.SaveChanges();
        }
        protected BaseDao(DbContext _context)
        {
            _context.Database.EnsureCreated();
            Context = _context;
            Table = Context.Set<T>();
        }
        public bool Insert(T model)
        {
            Context.Entry(model).State = EntityState.Added;
            return SaveChanges() > 0;
        }
        public async Task InsertAsync(T model)
        {
            Context.Entry(model).State = EntityState.Added;
            await Context.SaveChangesAsync();
        }
        public bool Update(T model)
        {
            Context.Entry(model).State = EntityState.Modified;
            return Context.SaveChanges() > 0;
        }
        public bool Delete(T model)
        {
            Context.Entry(model).State = EntityState.Deleted;
            return Context.SaveChanges() > 0;
        }
        public bool Delete(List<T> models)
        {
            foreach (var item in models)
            {
                Context.Entry(item).State = EntityState.Deleted;
            }
            return Context.SaveChanges() > 0;
        }
        public bool Delete(long id)
        {
            return Delete(Table.Find(id));
        }
        public T Get(long id)
        {
            return Table.Find(id);
        }
        public IQueryable<T> GetAll()
        {
            return Table;
        }
        public void Detach(T model)
        {
            Context.Entry(model).State = EntityState.Detached;
        }
    }
}