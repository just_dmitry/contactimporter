﻿using ContactImporter.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ContactImporter.Data.Dao
{
    public class FileProcessStatsDao : BaseDao<FileProcessStats>, IDisposable
    {
        public FileProcessStatsDao(DbContext _context) : base(_context)
        {
        }
        public FileProcessStats GetByGuid(string guid)
        {
            return Table.FirstOrDefault(f => Guid.Parse(guid).Equals(f.Guid));
        }
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}