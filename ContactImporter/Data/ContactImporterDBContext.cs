﻿using ContactImporter.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace ContactImporter.Data
{
    public class ContactImporterDBContext: DbContext
    {
        public ContactImporterDBContext(DbContextOptions<ContactImporterDBContext> options) : base(options)
        {
        }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<FileProcessStats> FileProcessStats { get; set; }
    }
}